# gleams.io entry test
This serves as entry test for gleams.io

## BadgeEx
You can use this to display badge, receive `icon` props, and the value of the badge can be written as a child.
```
<script setup lang="ts">
import BadgeEx from "./components/BadgeEx.vue";
import icoDiamond from "./assets/ico-diamond.png";
</script>

<template>
  <BadgeEx :icon="icoDiamond">3</BadgeEx>
</template>
```

## ButtonEx
### Example of using icon
Button text can be written as child component. If you want to add image to your button, you can import the image from the main component and send it as properties.

```
<script setup lang="ts">
import ButtonEx from "./components/ButtonEx.vue";
import icoPlus from "./assets/ico-plus.png";
</script>

<template>
  <ButtonEx :icon="icoPlus">Button Text</ButtonEx>
</template>
```

### Example of using isLoading
Button text can be written as child component. If `isLoading` property is set to `true`, then loading indicator will be shown.
```
<script setup lang="ts">
import ButtonEx from "./components/ButtonEx.vue";
</script>

<template>
  <ButtonEx :isLoading="true">Button Text</ButtonEx>
</template>
```

### Using other component inside button as an indicator
You can insert other component, for example, `BadgeEx` to indicate something within your button. 
```
<script setup lang="ts">
import ButtonEx from "./components/ButtonEx.vue";
import icoPlus from "./assets/ico-plus.png";
</script>

<template>
  <ButtonEx class="hover" :icon="icoPlus">
    <span>Button</span>
    <template v-slot:indicator>
      <BadgeEx :icon="icoDiamond">3</BadgeEx>
    </template>
  </ButtonEx>
</template>
```

## Result Examples
![output example](./docs/sample.png)

```
<script setup lang="ts">
import ButtonEx from "./components/ButtonEx.vue";
import BadgeEx from "./components/BadgeEx.vue";
import icoPlus from "./assets/ico-plus.png";
import icoDiamond from "./assets/ico-diamond.png";
</script>

<template>
  <table>
    <tr>
      <th>Button State</th>
      <th>Standard</th>
      <th>Hover</th>
      <th>Active</th>
      <th>Loading</th>
      <th>Focus</th>
      <th>Disabled</th>
    </tr>
    <tr>
      <td>Default</td>
      <td>
        <ButtonEx>Button</ButtonEx>
      </td>
      <td>
        <ButtonEx class="hover">Button</ButtonEx>
      </td>
      <td>
        <ButtonEx class="active">Button</ButtonEx>
      </td>
      <td>
        <ButtonEx :isLoading="true">Button</ButtonEx>
      </td>
      <td>
        <ButtonEx class="focus">Button</ButtonEx>
      </td>
      <td>
        <ButtonEx disabled>Button</ButtonEx>
      </td>
    </tr>
    <tr>
      <td>With Icon</td>
      <td>
        <ButtonEx :icon="icoPlus">Button</ButtonEx>
      </td>
      <td>
        <ButtonEx class="hover" :icon="icoPlus">Button</ButtonEx>
      </td>
      <td>
        <ButtonEx class="active" :icon="icoPlus">Button</ButtonEx>
      </td>
      <td>
        <ButtonEx :isLoading="true" :icon="icoPlus">Button</ButtonEx>
      </td>
      <td>
        <ButtonEx class="focus" :icon="icoPlus">Button</ButtonEx>
      </td>
      <td>
        <ButtonEx disabled :icon="icoPlus">Button</ButtonEx>
      </td>
    </tr>
    <tr>
      <td>Icon Only</td>
      <td>
        <ButtonEx :icon="icoPlus" />
      </td>
      <td>
        <ButtonEx class="hover" :icon="icoPlus" />
      </td>
      <td>
        <ButtonEx class="active" :icon="icoPlus" />
      </td>
      <td>
        <ButtonEx :isLoading="true" :icon="icoPlus" />
      </td>
      <td>
        <ButtonEx class="focus" :icon="icoPlus" />
      </td>
      <td>
        <ButtonEx disabled :icon="icoPlus">Button</ButtonEx>
      </td>
    </tr>
    <tr>
      <td>With Badge + Icon</td>
      <td>
        <ButtonEx :icon="icoPlus">
          <span>Button</span>
          <template v-slot:indicator>
            <BadgeEx :icon="icoDiamond">3</BadgeEx>
          </template>
        </ButtonEx>
      </td>
      <td>
        <ButtonEx class="hover" :icon="icoPlus">
          <span>Button</span>
          <template v-slot:indicator>
            <BadgeEx :icon="icoDiamond">3</BadgeEx>
          </template>
        </ButtonEx>
      </td>
      <td>
        <ButtonEx class="active" :icon="icoPlus">
          <span>Button</span>
          <template v-slot:indicator>
            <BadgeEx :icon="icoDiamond">3</BadgeEx>
          </template>
        </ButtonEx>
      </td>
      <td>
        <ButtonEx :isLoading="true" :icon="icoPlus">
          <span>Button</span>
          <template v-slot:indicator>
            <BadgeEx :icon="icoDiamond">3</BadgeEx>
          </template>
        </ButtonEx>
      </td>
      <td>
        <ButtonEx class="focus" :icon="icoPlus">
          <span>Button</span>
          <template v-slot:indicator>
            <BadgeEx :icon="icoDiamond">3</BadgeEx>
          </template>
        </ButtonEx>
      </td>
      <td>
        <ButtonEx disabled :icon="icoPlus">
          <span>Button</span>
          <template v-slot:indicator>
            <BadgeEx :icon="icoDiamond">3</BadgeEx>
          </template>
        </ButtonEx>
      </td>
    </tr>
    <tr>
      <td>Icon Only + Badge</td>
      <td>
        <ButtonEx :icon="icoPlus">
          <template v-slot:indicator>
            <BadgeEx :icon="icoDiamond">1</BadgeEx>
          </template>
        </ButtonEx>
      </td>
      <td>
        <ButtonEx class="hover" :icon="icoPlus">
          <template v-slot:indicator>
            <BadgeEx :icon="icoDiamond">2</BadgeEx>
          </template>
        </ButtonEx>
      </td>
      <td>
        <ButtonEx class="active" :icon="icoPlus">
          <template v-slot:indicator>
            <BadgeEx :icon="icoDiamond">3</BadgeEx>
          </template>
        </ButtonEx>
      </td>
      <td>
        <ButtonEx :isLoading="true" :icon="icoPlus">
          <template v-slot:indicator>
            <BadgeEx :icon="icoDiamond">4</BadgeEx>
          </template>
        </ButtonEx>
      </td>
      <td>
        <ButtonEx class="focus" :icon="icoPlus">
          <template v-slot:indicator>
            <BadgeEx :icon="icoDiamond">5</BadgeEx>
          </template>
        </ButtonEx>
      </td>
      <td>
        <ButtonEx disabled :icon="icoPlus">
          <template v-slot:indicator>
            <BadgeEx :icon="icoDiamond">5</BadgeEx>
          </template>
        </ButtonEx>
      </td>
    </tr>
  </table>
</template>
```

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
