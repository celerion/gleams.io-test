import { describe, it, expect } from "vitest";

import { mount } from "@vue/test-utils";
import ButtonEx from "../ButtonEx.vue";

describe("ButtonEx", () => {
  it("default button renders correctly on standard state", () => {
    const wrapper = mount(ButtonEx, {
      slots: {
        default: "Button Example",
      },
    });
    expect(wrapper.vm.haveSlot).toEqual(true);
    expect(wrapper.vm.haveIndicator).toEqual(false);
    expect(wrapper.vm.buttonClasses).toEqual({
      "c-button": true,
      wide: true,
    });
    expect(wrapper.vm.buttonIconClasses).toEqual({
      "c-button__icon": true,
    });
    expect(wrapper.vm.buttonSpinnerClasses).toEqual({
      "c-button__spinner": true,
      "ml-14": true,
    });
    expect(wrapper.vm.shouldShowIcon).toEqual(false);
    expect(wrapper.vm.shouldShowFrontSpinnerIcon).toEqual(false);
    expect(wrapper.vm.shouldShowIndicator).toEqual(false);
    expect(wrapper.element).toMatchSnapshot();
  });
  it("default button renders correctly on is loading state", () => {
    const wrapper = mount(ButtonEx, {
      props: {
        isLoading: true,
      },
      slots: {
        default: "Button Example",
      },
    });
    expect(wrapper.vm.haveSlot).toEqual(true);
    expect(wrapper.vm.haveIndicator).toEqual(false);
    expect(wrapper.vm.buttonClasses).toEqual({
      "c-button": true,
      wide: true,
    });
    expect(wrapper.vm.buttonIconClasses).toEqual({
      "c-button__icon": true,
    });
    expect(wrapper.vm.buttonSpinnerClasses).toEqual({
      "c-button__spinner": true,
      "ml-14": true,
    });
    expect(wrapper.vm.shouldShowIcon).toEqual(false);
    expect(wrapper.vm.shouldShowFrontSpinnerIcon).toEqual(false);
    expect(wrapper.vm.shouldShowIndicator).toEqual(false);
    expect(wrapper.element).toMatchSnapshot();
  });
  it("with icon button renders correctly on default state", () => {
    const wrapper = mount(ButtonEx, {
      props: {
        icon: "something",
        isLoading: true,
      },
      slots: {
        default: "Button Example",
      },
    });
    expect(wrapper.vm.haveSlot).toEqual(true);
    expect(wrapper.vm.haveIndicator).toEqual(false);
    expect(wrapper.vm.buttonClasses).toEqual({
      "c-button": true,
      wide: true,
    });
    expect(wrapper.vm.buttonIconClasses).toEqual({
      "c-button__icon": true,
    });
    expect(wrapper.vm.buttonSpinnerClasses).toEqual({
      "c-button__spinner": true,
      "ml-14": true,
    });
    expect(wrapper.vm.shouldShowIcon).toEqual(true);
    expect(wrapper.vm.shouldShowFrontSpinnerIcon).toEqual(false);
    expect(wrapper.vm.shouldShowIndicator).toEqual(false);
    expect(wrapper.element).toMatchSnapshot();
  });
});
